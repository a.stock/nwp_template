
!------------------------------------------------------------------------------!
! Numerical Weather Prediction - Programming Excercise
!------------------------------------------------------------------------------!
! Tolles Programm für das Praktikum

! TODO: Add description
!  Add a proper description of program. Should include how to steer (including
!  required input) and what to expect as output.
!  2018-02-14, TG

!------------------------------------------------------------------------------!
! Author: Annika Stock
! Date  : 2019-02-4
!------------------------------------------------------------------------------!
! NOTE: Code compilation
!  When compiling the code, NetCDF libraries must be included/linked like so
!    ifort nwp_program.f90 -I <path-to-netcdf>/include -L <path-to-netcdf>/lib -lnetcdff
!  2018-02-07, TG

PROGRAM nwp_program

   USE netcdf

   IMPLICIT NONE


   !- Declare variables
   CHARACTER(LEN=100) :: file_out !(namelist param)
   CHARACTER(LEN=100) :: file_era !(namelist parameter)
   
   INTEGER :: i            !< Laufindex
   INTEGER :: j            !< Laufindex
   INTEGER :: k            !< Laufindex
   INTEGER :: t            !< Laufindex
   INTEGER :: tt           !< Zeitindex des Referenz- Geopotentials
   INTEGER :: k_max        !< Maximale Itterationen der SOR-Mathode (namelist param)
   INTEGER :: nx           !< Gitterpunkte entlang x (namelist param)
   INTEGER :: ny           !< Gitterpunkte entlang y (namelist param)
   INTEGER :: nt           !< Anzahl der Zeitschritte
   INTEGER :: nt_ref       !< Anzahl der Zeitschritte des Globalmodells
   INTEGER :: ioerr        !< Fehlermeldung 
   
   REAL :: beta            !< beta Parameter
   REAL :: delta           !< Gitterweite (namelist param)
   REAL :: dt              !< Zeitschritt
   REAL :: dt_do           !< Zeitschritt für die Ausgabe
   REAL :: f_0 = 7.27E-5   !< Coriolisparameter bei y=0, phi = 30°
   REAL :: latitude        !< Breitengrad in Grad (namelist parameter)
   REAL :: phi_0           !< Breitengrad in Rad
   REAL :: omega           !< relaxation factor of SOR method
   REAL :: t_end           !< zu simulierende Zeit (s)
   REAL :: t_sim           !< Simulierte Zeit (s)
   REAL :: time_end        !< zu simulierende Zeit (h, namelist param)
   REAL :: time_dt_do      !< Zeitintervall der Output-Datei (h, namelist param)
   REAL :: t_ref           !<  Referenzzeit
   REAL :: u_bg            !< background wind speed
   REAL :: u_max           !< Maximum der Windegschwindigkeit

   REAL :: a = 6367490     !< Radius der Erde in m
   REAL :: omega_f         !< Winkelgeschwindigkeit
   REAL :: c_t             !< Verlagerungsgeschwindigkeit theoretisch
   REAL :: c_m             !< Verlagerungsgeschwindigkeit modell
   
 !- Parameter zur Berechnung der Laufzeit des Programs 
   INTEGER :: count         !<
   INTEGER :: count_rate    !<
   REAL :: SOR              !< für SOR-Methode
   REAL :: VOR              !< für Zeitschrittverfahren
   REAL :: Wind             !< für Windgeschwindigkeiten
   REAL :: DAT              !< für Datenausgabe
   REAL :: GES              !< für Gesamtlaufzeit 


   REAL, DIMENSION(:), ALLOCATABLE :: f         !< Coriolisparameter entlang y

   REAL, DIMENSION(:,:), ALLOCATABLE :: phi     !< Geopotential
   REAL, DIMENSION(:,:,:), ALLOCATABLE :: phi_ref !< Referenz Geopotential
   REAL, DIMENSION(:,:), ALLOCATABLE :: u       !< Windegschwindigkeit entlang x
   REAL, DIMENSION(:,:), ALLOCATABLE :: v       !< Windegschwindigkeit entlang y
   REAL, DIMENSION(:,:), ALLOCATABLE :: zeta    !< Vorticity zum Zeitpunkt t
   REAL, DIMENSION(:,:), ALLOCATABLE :: zeta_m  !< Vorticity zum Zeitpunkt t-1
   REAL, DIMENSION(:,:), ALLOCATABLE :: zeta_p  !< Vorticity zum Zeitpunkt t+1
   
   REAL, DIMENSION(2) :: m1     !< Position des Maximums von phi bei t = 0
   REAL, DIMENSION(2) :: m2     !< Position des Maximums von phi bei t = 48

   REAL, PARAMETER :: g = 9.81      !< Erdbeschleunigung
   REAL, PARAMETER :: pi = 3.14159  !< Kreiszahl Pi

 !- Definiere und lese init-namelist
   namelist / INIT / delta, file_out, k_max, latitude, nx, ny, time_dt_do,  &
                     time_end, latitude, file_era
                     
                     
    CALL SYSTEM_CLOCK(count, count_rate) ! Starten der Berechnung der Gesamtlaufzeit        
        GES = REAL(count)/REAL(count_rate)                   
                  
                     
 !- Start
   WRITE(*, '(A//A)') ' --- NWP program, V0.0.1 ---',  &
                      ' --- Start Wettervorhersage für den Zeitraum vom 24.10.2017 0 UTC bis 26.10.2017 0 UTC...'
    
    
   
 !- Öffnen der Namelist
   OPEN(1, FILE='./nwp_prog.init', STATUS='OLD', FORM='FORMATTED', IOSTAT=ioerr)
   IF (ioerr /= 0) THEN
      CALL error_message('err11') !Sollte ein Fehler beim Öffnen auftreten, dann aufrufen der Subroutine mit error message 
   ENDIF

 !- Lesen der Namelist
   READ(1, NML=INIT, IOSTAT=ioerr)
   IF (ioerr /= 0) THEN    !
      CALL error_message('err12') !Sollte ein Fehler beim Lesen auftreten, dann aufrufen der Subroutine mit error message
   ENDIF

   CLOSE(1)
 
 !- Bestimmung der Zeitschritte für das Referenz-Geopotential
   nt_ref = INT(time_end/6.0)

 !- Allocieren der einzelnen Vektoren 
    ALLOCATE(phi(-1:ny+1,-1:nx+1))
    ALLOCATE(phi_ref(-1:ny+1,-1:nx+1,0:nt_ref))
    ALLOCATE(zeta(-1:ny+1,-1:nx+1))
    ALLOCATE(zeta_m(-1:ny+1,-1:nx+1))
    ALLOCATE(zeta_p(-1:ny+1,-1:nx+1))
    ALLOCATE(f(0:ny))
    ALLOCATE(u(-1:ny+1, -1:nx+1))
    ALLOCATE(v(-1:ny+1, -1:nx+1))
    
 !- Umrechnung der Endzeit von Stunden in Sekunden 
   t_end = time_end * 3600.0 ![t_end]=s
   
 !- Festlegung der maximalen Windgeschwindigkeit
   u_max = 10.0      ![umax]=m/s
   
    !Initialisierung der Startwerte
    f(:) = 0.0
    phi(:,:) = 0.0
    phi_ref(:,:,:) = 0.0
    zeta(:,:) = 0.0
    zeta_m(:,:) = 0.0
    zeta_p(:,:) = 0.0

 !- Berechnung des variierenden Coriolisparameters auf der Beta-Ebene        
 
    omega_f = 2*pi / 81646 ! Berechnung der Winkelgeschwindigkeit 1/s
    phi_0 = latitude * pi /180 !Umrechnung von Grad in Rad
    beta = 2/a * omega_f * COS(phi_0) !Berechnung von Beta in 1/ms

    DO j = 0, ny   !Der Coriolisparameter varrieert auf der Beta-Ebene
        f(j) = f_0 + beta * delta * j !in 1/s 
    END DO
    
    
 !- Berechnung der theoretischen Verlagerungsgeschwindigkeit
     !  lx = nx * delta
     !  ly = 2.0 * ny * delta
     !  u_bg = 0.0  
     !  c_t = u_bg - beta * 1.0 / ((2.0 * pi / lx)**2 + (2.0 * pi / ly)**2)
        
     
 !- Einlesen der Input-Datei
    CALL read_input_data('/home/gronemeier/Public/NWP/era_int_eu_20171024_20171026.nc')   
   
 !- Öffne Output-file
    CALL data_output('open', file_out)   

 !- Startwert für das Geopotential 
    phi(:,:) = phi_ref(:,:,0) 
    
 !- Berechne Startwert für Vorticity       
    DO i=0,nx  
        DO j= 0,ny 
            zeta(j,i) = (1/f(j)) * ((phi(j,i+1) - 2.0 * phi(j,i) + &
                        phi(j,i-1))/(delta**2) + (phi(j+1,i) - 2.0 * &
                        phi(j,i) + phi(j-1,i)) /(delta **2))                  
        END DO
    END DO  
       
    CALL get_wind
    
    CALL data_output('write',file_out)
          
 !- Start Vorhersage, Startwerte für Simualtionszeit und Anzahl der Zeitschritte
   t_sim = 0.0
   nt = 0
 !- Berechne Maximum des Geopotentials zum Zeitschritt t = 0  
   m1 = MAXLOC(phi)

   DO WHILE (t_sim <= t_end)
    
        dt = timestep(u, v)
       
        nt = nt + 1
        
        t_ref = INT(t_sim /(time_dt_do*3600.0))
        
        WRITE(*,*), 'Zeitschritt', nt 
        WRITE(*,*), 'Simulationszeit', t_sim/3600, 'h'
        WRITE(*,*) 'dt =' , dt/60 , 'min'
        CALL get_randwerte
    
        CALL get_vorticity    !Zeitliche Variation der Vorticity 
        
        CALL get_geopot(k_max) !Zeitliche Variation des Geopotentials 
        
        CALL get_wind()
        
        t_sim = t_sim + dt
        
        dt_do = INT((t_sim /(time_dt_do *3600 )))
        
        IF ( dt_do /= t_ref) THEN
        
            CALL data_output('write',file_out)
   
        END IF
        
        t_ref = dt_do
        
    END DO
  
 !- Berechne Maximum des Geopotentials bei t = 48 
    m2 = MAXLOC(phi)

 !- Berechnung der Verlagerungsgeschwindigkeit aus dem Modell
    c_m = (m2(2)-m1(2))*delta/(24.0*3600.0)

    WRITE (*,*), 'c_m', c_m
   
 !- Schließe output
  CALL data_output('close',file_out)
  
  
  CALL SYSTEM_CLOCK(count,count_rate)
    GES = REAL(count)/REAL(count_rate) - GES
  
  WRITE(*,*), 'Gesamte Laufzeit', GES, 's'
  WRITE(*,*), 'Laufzeit für Berechnung des Geopototentials', SOR, 's', &
            'Anteil an Gesamtlaufzeit', SOR/GES *100, '%'
  WRITE(*,*), 'Laufzeit für Berechnung der Vorticity', VOR, 's', &
               'Anteil an Gesamtlaufzeit', VOR/GES *100, '%'
  WRITE(*,*), 'Laufzeit für Berechnung der Windgeschwindigkeiten',Wind, 's', &
                'Anteil an Gesamtlaufzeit', Wind/GES *100, '%'
  WRITE(*,*),'Laufzeit für Berechnung der Datenausgabe', DAT, 's', &
               'Anteil an Gesamtlaufzeit', DAT/GES *100, '%'
 
 !- Programende
  WRITE(*, '(A)') ' --- Vorhersage beendet ---'


CONTAINS

 !- Functions and subroutines

   REAL FUNCTION timestep(u, v)
    !Berechne Zeitschritt
    
    REAL, DIMENSION(2) :: cfl_min 
    REAL, DIMENSION(0:ny,-1:nx+1), INTENT(IN) :: u
    REAL, DIMENSION(0:ny,-1:nx+1), INTENT(IN) :: v 

      cfl_min(1) = delta/MAXVAL(ABS(u))
      cfl_min(2) = delta/MAXVAL(ABS(v))
      timestep= 0.5 * MINVAL(cfl_min) - 1E-06
      
   END FUNCTION timestep

   !--------------------------------------------------------------------------

   SUBROUTINE error_message(fehler)
   ! Beim Auftreten eines Fehlers mit der Namelist, soll eine Error-Message geschrieben und das Programm geschlossen werden
   
    CHARACTER(LEN=*), INTENT(IN) :: fehler
    
    SELECT CASE(fehler) ! Die Auswahl welche Fehlermeldung auftritt erfolgt 
                        ! über CASE-Abfrage
        CASE('err11')
        WRITE(*,*) 'Es ist ein Fehler, beim öffnen der Namelist, aufgetreten'
        STOP
        
        CASE('err12') 
        WRITE(*,*) 'Es ist ein Fehler, beim lesen der Namelist, aufgetreten'
        STOP
        
    END SELECT 
   
   END SUBROUTINE error_message

   !--------------------------------------------------------------------------

   SUBROUTINE get_geopot(max_it)
   !- Berechnung des Geopotentials via SOR-Methode
        
        INTEGER, INTENT (IN) :: max_it  !< Anzahl der maximalen Itterationen
        
        REAL :: eps_grenz = 0.001
        REAL :: eps
        REAL::  epsalt
        REAL :: diff    !< Varible für Zeitmessung

        REAL, DIMENSION (:,:), ALLOCATABLE :: phialt !Wert phi für vorherigen Schritt
  
        ALLOCATE(phialt(-1:ny+1,-1:nx+1))
        
        CALL SYSTEM_CLOCK(count, count_rate) !Berechnung der Laufzeit für die Subroutine get_geopot
            diff = REAL(count)/REAL(count_rate)  
        
        omega = 2.0 - 2.0 * pi / SQRT(2.0)             &
                * SQRT( (1.0 /(nx + 1.0)**2)           &
                    + (1.0 /(ny + 1.0)**2))

        
        DO k= 1, k_max    
            DO i = 0,nx
                Do j = 0,ny     
                    phi(j,i) = phi(j,i) * (1.0-omega) + omega/4.0  * &
                                (phi(j,i+1) + phi(j,i-1) + phi(j+1,i) + & 
                                phi(j-1,i) - f(j) * delta**2 * zeta(j,i))
                END DO
            END DO
                   
            
            eps = MAXVAL(ABS(phi - phialt)) !Kriterium  ausreichende Konvergenz		
        
           
            IF (eps <= epsalt .AND. eps <= eps_grenz) THEN !Abbruchkrieterium
                WRITE(*,*), 'Iterationsschritte', t
                EXIT
            END IF

    !- Tauschen der Werte für den nächsten Iterationsschritt
            epsalt= eps
            phialt = phi
                        
        END DO    
        
        CALL SYSTEM_CLOCK(count, count_rate) 
            diff = REAL(count)/REAL(count_rate) - diff 
        
        SOR = SOR + diff  ! Aufadieren der Laufzeit für die einzelnen Zeitschritte  
    
   END SUBROUTINE get_geopot

   !----------------------------------------------------------------------------

   SUBROUTINE get_vorticity
    !- Berechnung der Vorticity über Euler-Verfahren für den ersten Zeitschritt, für die restlichen Zeitschritte über Leap-frog-Verfahren
   
    REAL :: diff    !< Varible für Zeitmessung
   
    CALL SYSTEM_CLOCK(count, count_rate) !Start der Berechnung der Laufzeit für die Vorticity
    diff = REAL(count)/REAL(count_rate)
    
    SELECT CASE(nt) !Auswählen des Zeitschritts/ Zeitschrittverfahrens
        !- Euler-Verfahren
        CASE(1)
            WRITE(*,*), 'Euler-Verfahren'
            DO i= 1, nx-1
                DO j= 1,ny-1
                    zeta_p(j,i) = zeta(j,i) - dt * (u(j,i) * &
                                (zeta(j,i+1)- zeta(j,i-1))/ (2.0 * delta) & 
                                + v(j,i) *  & 
                                (zeta(j+1,i) - zeta(j-1,i))/ (2.0 * delta) &
                                +v(j,i) * beta)  
                END DO
            END DO   
            
        !- Leap-Frog-Verfahren 
        CASE(2:)
            WRITE(*,*),'Leap-Frog-Verfahren'
            DO i= 1,nx-1
                DO j= 1,ny-1
                    zeta_p(j,i) = zeta_m(j,i) - 2.0 * dt * (u(j,i) * & 
                                    (zeta(j,i+1)- zeta(j,i-1))/(2.0 * delta) & 
                                    + v(j,i) * & 
                                    (zeta(j+1,i) - zeta(j-1,i))/(2.0 * delta) &
                                    +v(j,i) * beta)
                END DO
            END DO
            
        END SELECT 
        
        zeta_m = zeta   !- Tauschen der Werte für nächsten Zeitschritt
        zeta = zeta_p
        
        CALL SYSTEM_CLOCK(count, count_rate)
            diff = REAL(count)/REAL(count_rate) - diff
        
        VOR = VOR + diff ! Aufadieren der Laufzeit für die einzelnen Zeitschritte
 
   END SUBROUTINE get_vorticity
   
   !----------------------------------------------------------------------------

   SUBROUTINE get_wind()
   !- Berechnung der Windegschwindigkeitskomponenten über geostrophische Windrelation
   
    REAL :: diff    !< Varible für Zeitmessung
   
    CALL SYSTEM_CLOCK(count, count_rate) !Start der Berechnung der Laufzeit für den Wind
        diff = REAL(count)/REAL(count_rate)
    
    !- Berechnung der Windgeschwindigkeiten
    DO i= 0, nx
            DO j= 0,ny 
                u(j,i) = -1.0/f(j) * ((phi(j+1,i) - phi(j-1,i)) / (2.0*delta))
                v(j,i) =  1.0/f(j) * ((phi(j,i+1) - phi(j,i-1)) / (2.0*delta)) 
        END DO
    END DO   

    
     CALL SYSTEM_CLOCK(count, count_rate)
        diff = REAL(count)/REAL(count_rate) - diff
     
     Wind = Wind + diff ! Aufadieren der Laufzeit für die einzelnen Zeitschritte
 
     
   END SUBROUTINE get_wind

   !----------------------------------------------------------------------------

   SUBROUTINE read_input_data(input)
   !- Einlesen der Input-Datei vom Mesoskaligen Modell
   
      CHARACTER(LEN=*), INTENT(IN) :: input  !< Name der Input-Datei

      INTEGER, SAVE :: id_file         !< ID der NetCDF Datei
      INTEGER, SAVE :: id_var_in       !< ID des Input Arrays
      INTEGER, SAVE :: nc_stat         !< Status flag of NetCDF routines

     REAL, DIMENSION(:,:,:), ALLOCATABLE :: var_in !< Input Arraay
     
     ALLOCATE(var_in(-1:nx+1,-1:ny+1,0:nt_ref))
    
     WRITE(*, '(A)') ' --- Einlesen der Input-Datei...'

      !- Öffne NetCDF file
      nc_stat = NF90_OPEN( input, NF90_NOWRITE, id_file )
      IF ( nc_stat /= 0 )  WRITE(*,*) NF90_STRERROR(nc_stat)

      !- Aufrufen der ID des Arrays
      nc_stat = NF90_INQ_VARID( id_file, "phi", id_var_in )
      IF ( nc_stat /= 0 )  WRITE(*,*) NF90_STRERROR(nc_stat)

      !- Lese Array
      nc_stat = NF90_GET_VAR( id_file, id_var_in, var_in,        &
                            start = (/ 1, 1, 1 /),               &
                            count = (/ nx+3, ny+3, nt_ref+1 /) )
                            
                            WRITE (*,*) nx+3, ny+3, nt_ref+1
      IF ( nc_stat /= 0 )  WRITE(*,*) NF90_STRERROR(nc_stat)

      !- Abspeichern auf Referenz-Geopotential
      DO  t = 0, nt_ref
         DO  i = -1, nx+1
            DO  j = -1, ny+1
               phi_ref(j,i,t) = var_in(i,j,t)
            ENDDO
         ENDDO
         
      ENDDO

      WRITE(*, '(A)') '---Fertig!---'
      
   END SUBROUTINE read_input_data

   !----------------------------------------------------------------------------
    SUBROUTINE get_randwerte
    !- Bestimmen der Randwerte für das Geopotential und die Vorticity
    
        REAL, DIMENSION(:,:), ALLOCATABLE :: zeta_r   !< Vorticity für den Rand

        ALLOCATE(zeta_r(0:ny,0:nx))
        
        tt =  INT(t_sim/(6.0 * 3600.0))
      
    !- Bestimmung des Rands des Geopotentials bei y = 0
        DO i = -1, nx+1
            phi(0,i) = phi_ref(0,i,tt) + (phi_ref(0,i,tt+1)&
            - phi_ref(0,i,tt))/(21600.0) * (t_sim - (tt * 6.0 * 3600.0))
            
            phi(1,i) = phi_ref(1,i,tt) + (phi_ref(1,i,tt+1)&
            - phi_ref(1,i,tt))/(21600.0) * (t_sim - (tt * 6.0 * 3600.0))
            
            phi(-1,i) = phi_ref(-1,i,tt) + (phi_ref(-1,i,tt+1)&
            - phi_ref(-1,i,tt))/(21600.0) * (t_sim - (tt * 6.0 * 3600.0))
        END DO
        
     !- Bestimmtung des Rands des Geopotentials bei y = ny
        DO i = -1, nx+1
        
            phi(ny,i) = phi_ref(ny,i,tt) + (phi_ref(ny,i,tt+1) &
            - phi_ref(ny,i,tt))/(21600.0) * (t_sim - (tt * 6.0 * 3600.0))
            
            phi(ny+1,i) = phi_ref(ny+1,i,tt) + (phi_ref(ny+1,i,tt+1)&
            - phi_ref(ny+1,i,tt))/(21600.0) * (t_sim - (tt * 6.0 * 3600.0))
            
            phi(ny-1,i) = phi_ref(ny-1,i,tt) + (phi_ref(ny-1,i,tt+1)&
            - phi_ref(ny-1,i,tt))/(21600.0) * (t_sim - (tt * 6.0 * 3600.0))
        END DO    
        
    !- Bestimmung des Rands des Geopotentials bei x = 0
        DO j = -1, ny+1
            phi(j,0) = phi_ref(j,0,tt) + (phi_ref(j,0,tt+1)&
            - phi_ref(j,0,tt))/(21600.0) * (t_sim - (tt * 6.0 * 3600.0))
            
            phi(j,1) = phi_ref(j,1,tt) + (phi_ref(j,1,tt+1)&
            - phi_ref(j,1,tt))/(21600.0) * (t_sim - (tt * 6.0 * 3600.0))
            
            phi(j,-1) = phi_ref(j,-1,tt) + (phi_ref(j,-1,tt+1)&
            - phi_ref(j,-1,tt))/(21600.0) * (t_sim - (tt * 6.0 * 3600.0))
        END DO   
              
    !- Bestimmung des Rands des Geopotentials bei x = nx 
        DO j = -1, ny+1
            phi(j,nx) = phi_ref(j,nx,tt) + (phi_ref(j,nx,tt+1)&
            - phi_ref(j,nx,tt))/(21600.0) * (t_sim - (tt * 6.0 * 3600.0))
            
            phi(j,nx+1) = phi_ref(j,nx+1,tt) + (phi_ref(j,nx+1,tt+1)&
            - phi_ref(j,nx+1,tt))/(21600.0) * (t_sim - (tt * 6.0 * 3600.0))
            
            phi(j,nx-1) = phi_ref(j,nx-1,tt) + (phi_ref(j,nx-1,tt+1)&
            - phi_ref(j,nx-1,tt))/(21600.0) * (t_sim - (tt * 6.0 * 3600.0))
        END DO
    
    !- Bestimmung der Randwerte der Vorticity
        DO i = 0,nx
            DO j = 0,ny
                zeta_r(j,i) = (1/f(j)) * ((phi(j,i+1) - 2.0 * phi(j,i) &
                                + phi(j,i-1))/(delta**2) &
                                + (phi(j+1,i) - 2.0 * phi(j,i) + &
                                phi(j-1,i))/(delta**2))
            END DO
        END DO 
        
        zeta(0,0:nx) = zeta_r(0,:)
        zeta(ny,0:nx) = zeta_r(ny,:)
        zeta(0:ny,0) = zeta_r(:,0)
        zeta(0:ny,nx) = zeta_r(:,nx)
    
    
  
   END SUBROUTINE get_randwerte
   !----------------------------------------------------------------------------
   SUBROUTINE data_output(action, output)
   ! Data output to NetCDF file

      CHARACTER(LEN=*), INTENT(IN) :: action !< flag to steer routine (open/close/write)
      CHARACTER(LEN=*), INTENT(IN) :: output !< output file name prefix

      INTEGER, SAVE :: do_count=0      !< counting output
      INTEGER, SAVE :: id_dim_time     !< ID of dimension time
      INTEGER, SAVE :: id_dim_x        !< ID of dimension x
      INTEGER, SAVE :: id_dim_y        !< ID of dimension time
      INTEGER, SAVE :: id_file         !< ID of NetCDF file
      INTEGER, SAVE :: id_var_phi      !< ID of geopotential
      INTEGER, SAVE :: id_var_phiref   !< ID of reference geopotential
      INTEGER, SAVE :: id_var_time     !< ID of time
      INTEGER, SAVE :: id_var_u        !< ID of wind speed along x
      INTEGER, SAVE :: id_var_v        !< ID of wind speed along y
      INTEGER, SAVE :: id_var_x        !< ID of x
      INTEGER, SAVE :: id_var_y        !< ID of y
      INTEGER, SAVE :: id_var_zeta     !< ID of vorticity
      INTEGER, SAVE :: nc_stat         !< status flag of NetCDF routines
      
      REAL :: diff  !< Varible für Zeitmessung

      REAL, DIMENSION(:), ALLOCATABLE :: netcdf_data_1d  !< 1D output array

      REAL, DIMENSION(:,:), ALLOCATABLE :: netcdf_data_2d  !< 2D output array

    CALL SYSTEM_CLOCK(count,count_rate)
        diff = REAL(count)/REAL(count_rate) 
            
            
      SELECT CASE (TRIM(action))

         !- Initialize output file
         CASE ('open')

            !- Delete any pre-existing output file
            OPEN(20, FILE=TRIM(output)//'.nc')
            CLOSE(20, STATUS='DELETE')

            !- Open file
            nc_stat = NF90_CREATE(TRIM(output)//'.nc', NF90_NOCLOBBER, id_file)
            IF (nc_stat /= NF90_NOERR)  PRINT*, '+++ netcdf error'

            !- Write global attributes
            nc_stat = NF90_PUT_ATT(id_file, NF90_GLOBAL,  &
                                    'Conventions', 'COARDS')
            nc_stat = NF90_PUT_ATT(id_file, NF90_GLOBAL,  &
                                    'title', 'barotropic nwp-model')

            !- Define time coordinate
            nc_stat = NF90_DEF_DIM(id_file, 'time', NF90_UNLIMITED,  &
                                    id_dim_time)
            nc_stat = NF90_DEF_VAR(id_file, 'time', NF90_DOUBLE,  &
                                    id_dim_time, id_var_time)
            nc_stat = NF90_PUT_ATT(id_file, id_var_time, 'units',  &
                                    'seconds since 1900-1-1 00:00:00')

            !- Define spatial coordinates
            nc_stat = NF90_DEF_DIM(id_file, 'x', nx+1, id_dim_x)
            nc_stat = NF90_DEF_VAR(id_file, 'x', NF90_DOUBLE,  &
                                    id_dim_x, id_var_x)
            nc_stat = NF90_PUT_ATT(id_file, id_var_x, 'units', 'meters')

            nc_stat = NF90_DEF_DIM(id_file, 'y', ny+1, id_dim_y)
            nc_stat = NF90_DEF_VAR(id_file, 'y', NF90_DOUBLE,  &
                                    id_dim_y, id_var_y)
            nc_stat = NF90_PUT_ATT(id_file, id_var_y, 'units', 'meters')

            !- Define output arrays
            nc_stat = NF90_DEF_VAR(id_file, 'phi', NF90_DOUBLE,           &
                                    (/id_dim_x, id_dim_y, id_dim_time/),  &
                                    id_var_phi)
            nc_stat = NF90_PUT_ATT(id_file, id_var_phi,  &
                                    'long_name', 'geopotential at 500hPa')
            nc_stat = NF90_PUT_ATT(id_file, id_var_phi,  &
                                    'short_name', 'geopotential')
            nc_stat = NF90_PUT_ATT(id_file, id_var_phi, 'units', 'm2/s2')

            nc_stat = NF90_DEF_VAR(id_file, 'phi_ref', NF90_DOUBLE,       &
                                    (/id_dim_x, id_dim_y, id_dim_time/),  &
                                    id_var_phiref)
            nc_stat = NF90_PUT_ATT(id_file, id_var_phiref,  &
                                    'long_name',            &
                                    'reference geopotential at 500hPa')
            nc_stat = NF90_PUT_ATT(id_file, id_var_phiref,  &
                                    'short_name', 'ref. geopotential')
            nc_stat = NF90_PUT_ATT(id_file, id_var_phiref, 'units', 'm2/s2')

            nc_stat = NF90_DEF_VAR(id_file, 'zeta', NF90_DOUBLE,          &
                                    (/id_dim_x, id_dim_y, id_dim_time/),  &
                                    id_var_zeta)
            nc_stat = NF90_PUT_ATT(id_file, id_var_zeta,  &
                                    'long_name', 'vorticity at 500hPa')
            nc_stat = NF90_PUT_ATT(id_file, id_var_zeta,  &
                                    'short_name', 'vorticity')
            nc_stat = NF90_PUT_ATT(id_file, id_var_zeta, 'units', '1/s')

            nc_stat = NF90_DEF_VAR(id_file, 'u', NF90_DOUBLE,             &
                                    (/id_dim_x, id_dim_y, id_dim_time/),  &
                                    id_var_u)
            nc_stat = NF90_PUT_ATT(id_file, id_var_u,  &
                                    'long_name',       &
                                    'u component of wind')
            nc_stat = NF90_PUT_ATT(id_file, id_var_u, 'short_name', 'u')
            nc_stat = NF90_PUT_ATT(id_file, id_var_u, 'units', 'm/s')

            nc_stat = NF90_DEF_VAR(id_file, 'v', NF90_DOUBLE,             &
                                    (/id_dim_x, id_dim_y, id_dim_time/),  &
                                    id_var_v)
            nc_stat = NF90_PUT_ATT(id_file, id_var_v,  &
                                    'long_name',       &
                                    'v component of wind')
            nc_stat = NF90_PUT_ATT(id_file, id_var_v, 'short_name', 'v')
            nc_stat = NF90_PUT_ATT(id_file, id_var_v, 'units', 'm/s')


            !- Leave define mode
            nc_stat = NF90_ENDDEF(id_file)

            !- Write axis to file
            ALLOCATE(netcdf_data_1d(0:nx))

            !- x axis
            DO  i = 0, nx
               netcdf_data_1d(i) = i * delta
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_x, netcdf_data_1d,  &
                                    start = (/1/), count = (/nx+1/))
            !- y axis
            DO  j = 0, ny
               netcdf_data_1d(j) = j * delta
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_y, netcdf_data_1d,  &
                                    start = (/1/), count = (/ny+1/))

            DEALLOCATE(netcdf_data_1d)


         !- Close NetCDF file
         CASE ('close')

            nc_stat = NF90_CLOSE(id_file)


         !- Write data arrays to file
         CASE ('write')

            ALLOCATE(netcdf_data_2d(0:nx,0:ny))

            do_count = do_count + 1

            !- Write time
            nc_stat = NF90_PUT_VAR(id_file, id_var_time, (/t_sim/),  &
                                    start = (/do_count/),               &
                                    count = (/1/))

            !- Write geopotential
            DO  i = 0, nx
               DO  j = 0, ny
                  netcdf_data_2d(i,j) = phi(j,i)
               ENDDO
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_phi, netcdf_data_2d,  &
                                    start = (/1, 1, do_count/),          &
                                    count = (/nx+1, ny+1, 1/))

            !- Write reference geopotential
            tt =  INT(t_sim/(6.0 * 3600.0))         
            DO  i = 0, nx
               DO  j = 0, ny
                  netcdf_data_2d(i,j) = phi_ref(j,i,tt) 
               ENDDO
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_phiref, netcdf_data_2d,  &
                                    start = (/1, 1, do_count/),             &
                                    count = (/nx+1, ny+1, 1/))

            !- Write vorticity
            DO  i = 0, nx
               DO  j = 0, ny
                  netcdf_data_2d(i,j) = zeta(j,i)
               ENDDO
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_zeta, netcdf_data_2d,  &
                                    start = (/1, 1, do_count/),           &
                                    count = (/nx+1, ny+1, 1/))

            !- Write u
            DO  i = 0, nx
               DO  j = 0, ny
                  netcdf_data_2d(i,j) = u(j,i)
               ENDDO
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_u, netcdf_data_2d,  &
                                    start = (/1, 1, do_count/),        &
                                    count = (/nx+1, ny+1, 1/))

            !- Write v
            DO  i = 0, nx
               DO  j = 0, ny
                  netcdf_data_2d(i,j) = v(j,i)
               ENDDO
            ENDDO
            nc_stat = NF90_PUT_VAR(id_file, id_var_v, netcdf_data_2d,  &
                                    start = (/1, 1, do_count/),        &
                                    count = (/nx+1, ny+1, 1/))

            DEALLOCATE(netcdf_data_2d)


         !- Print error message if unknown action selected
         CASE DEFAULT

            WRITE(*, '(A)') ' ** data_output: action "'//  &
                              TRIM(action)//'"unknown!'

      END SELECT
      
       CALL SYSTEM_CLOCK(count, count_rate)
        
     diff = REAL(count)/REAL(count_rate) - diff
     
     DAT = DAT + diff

   END SUBROUTINE data_output


END PROGRAM nwp_program
